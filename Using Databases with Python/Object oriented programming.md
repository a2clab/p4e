
### 1. Object oriented programming

Class - a template  
Method or message-A defined capability of a class  
Field or attribute - A bit of data in a class  
Object or instance - A particular instance of a class  

```
class PartyAnimal:

  x=0

  def party(self)
   self.x=self.x + 1
   print("So far",self.x)

an= PartyAnimal()

an.party()
an.party()
an.party()  


$python party1.py
Sofar 1
Sofar 2
Sofar 3
```

--------------------------

* dir() and type()   

  The dir() command lists the capabilities/methords.  
  Ignore the ones with underscores, these are used by python itself.  
  The rest are real operations that the objetcs can perform.  
  eg: dir(x)  

  The type() will tells us something *about* a variable  

  >>y= list()  
  >>type(y)  
  <type 'list'>  

--------------------------

* Object Lifecycle:  
--constructor  
--destructor  

```
def __init__(self):
    print('I am a constructor')


def __del__(self):
    print('I am destructed')
```
The constructor and destructor are optional. The constructor is typically used to set up variables.     
The destructor is seldom used.

------------------------------
Each instance has its own copy of the instance variable.

-------------------------------
Inheritance:

Subclasses are more specialized versions of a class,which inherit
attributes and behaviours from their parent classes,and can introduce their own


class PartyAnimal:  
aaa  
aaa  
aaa  


class FootballFan(PartyAnimal):   //this is like extending the parent calss  
bbbb  
bbbb  
-----------------------------------------







