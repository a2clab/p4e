### 1. Unicode Characters and Strings   

* In 1970, it was simple because there was pretty much one character set (didn't even have lowercase characters, they just had uppercase characters)  

* The problem that computers have is computers don't understand letters.What computers understand is numbers, and so we had to come up with a mapping between letters and numbers.  
* And so we came up with a mapping, and there's been many mappings historically.  
* The one of the most common mapping of the 1980s is this mapping called ASCII(The American Standard Code for Information Interchange) .
* And it says basically, this number equals this letter.
* http://www.asciitable.com/  


```
72 for H  
Somebody just decided that the capital H was going to be 72.
Lowercase e - 101
And newline is 10
So if you were really and truly going to look at what was going on inside the computer, it's storing these as numbers.
But the problem is there are 128 of these. Which means that you can't put every character in the world into a 0 through 128.
```

* Representing simple strings:  
  Each character is represented by a number between 0 and 255 stored in 8 bits of memory.    
  8 bits of memory= 1 byte of memory  
  3TB- 3 Terabytes of memory.  
  There is a function in python called  ord(), which stands for ordinal. This will tells us the numeric value of a simple ACCII character.    
  What's the ordinal? What is the number corresponding to H, and that's 72.    
  What's the number corresponding to lowercase e? It's 101. And what's the number corresponding to newline? And that is a 10. (Remember, newline is a single character.)    


* And in the early days, we would store every character in a byte of memory, otherwise known as eight bits of memory.  
  So the problem is in the old days we just had so few characters that we could put one character in a byte. And so the ord function tells us the numeric value of a simple ASCII character.
  So the e is 101 and capital H is 72. And then the newline which is here at the line feed(LF) which is 10.
  Now we could represent these in hexadecimal which is base 16 or octal which is base 8. Or actual binary which is what's really going on which is nothing but 0's and 1's.  


 * The real world is nothing like this.   
   There are all kinds of characters. And they had to come up with a scheme by which we could map these characters.  
   And for a while there were a whole bunch of incompatible ways to represent characters other than these ASCII, also known as Latin character sets,Arabic character sets. etc   
   These other character sets just completely invented their own way of representing.  
   And so you had these situations where Japanese computers couldn't talk to American computers or European computers at all.
   ie the Japanese computers just had their own way of representing characters. And the American computers had their own way of representing characters and they just couldn't talk.  

*  Finally they invented this thing called Unicode.   
   And so Unicode is this universal code for hundreds of millions of different characters and hundreds of different character sets.   
   So that instead of saying sorry, you don't fit with your language from some South Sea island, it's okay. We've got space in Unicode for that.   
   And so Unicode has lots and lots of characters, not just 128.  

*  In the early 2000s, the Internet came out and it became an important issue to have a way to exchange data.  
   Unicode helped a lot in that context    
   And so there's a couple of simple things that you might think are good ideas that turn out to be not such good ideas, although they're used.   
   So the first thing we did is these UTF-16, UTF-32 and UTF-8 are basically ways of representing a larger set of characters.

*  Multi-Byte characters:  
   To represent the wide range of characters computers must handle, we represent characters with more than one byte.
   UTF-16 - Fixed length- 2 bytes  
   UTF-32 - Fixed length- 4 bytes  
   UTF-8  - 1 to 4 bytes     
          UTF-8 is upward compatiable with ASCII, It has Automatic detection between ASCII and UTF-8  
          UTF-8 is recomended practice for encoding data to be exchanged between systems  


* In python 3 all strings are Unicode  
  Working with string variables in python programs and reading data from files usually works     
  But when we talk to a network resource using sockets or talk to a database we have to encode and decode data(usually to UTF-8)  

```
Inside python program string(Unicode)--->encode()-->Bytes UTF-8--->send()--->Socket-->network  
Network---->Socket--->recv()---->Bytes UTF-8---->decode()--->String Unicode(inside the python program)  
```