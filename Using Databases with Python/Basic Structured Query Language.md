### Basic SQL    

* In this we will learn the four core CRUD operations to manage data stored in a database.


* https://sqlitebrowser.org/ - SQLite is a relational database management system for small projetcs

* Relational Databases:  
  Relational Databases model data by storing rows and columns in tables.  
  The power of Relational Databases lies in the ability to efficiently retrieve data from those tables and in particular where there are multiple tables and the relationships between those tables invloved in the query.  

* DataBase: contains many tables  
* Relation(or table): contains tuples and attributes  
* Tuple(or row): a set of fields that generally represents an Object like a person or a music track  
* Attributes(also know as column or field)- one of possibly many elements of data corresponding to the object represented by the row.  

* Schema:strong contract about the contents    

* SQL: is the language we used to issue commands to the database  

* Create a table  
* Retrieve some data  
* Insert data  
* Update data  
* Delete data  
--------------

* Using Databases:     
  -Appln developer  
  -Database administrator: Database administrators (DBAs) use specialized software to store and    organize data.The role may include capacity planning, installation, configuration, database design, migration, performance monitoring, security, troubleshooting, as well as backup and data recovery.


* Database Model:  
A DB-model or DB-schema is the structure or format of a DB,described in a formal language supported by the DBMS,ie a DB model is the application  of a data model when used in conjunction with a DBMS  


* Common DBMS  
  Oracle- Large,commercial,enterprise-scale,very very tweakable. Used for enterprise kind of applns    
  MySql- Simple but very fast and scalable-Open source . Used for websites    
  Sqlserver- Very nice-From Microsoft     

* Many other smaller projects,free and opensource :   
  HSQL,Postgress(more similar to oracle),SQLite(it is very light and it an embedded software(mySql is a whole piece of of a separate software which talks to across network connections but SqlLight is a part of software and it is built in). Fancy music player in the car is using sql lite,Iphone is having for eg 30 copies of sql light inside it.)    
-----------



